require 'test_helper'

class NewsItemTest < ActiveSupport::TestCase
  test "the truth" do
    assert true
  end

  test "news item attributes must not be empty" do
    news_item = NewsItem.new
    assert news_item.invalid?
    assert news_item.errors[:published_on].any?
    assert news_item.errors[:content].any?
    assert news_item.errors[:image_url].any?
  end

  def new_news_item(image_url)
    NewsItem.new(content: "yyy",
    published_on: DateTime.new(2009,10,15,12),
    image_url: image_url)
  end

  test "image url" do
    ok = %w{ fred.gif fred.jpg fred.png FRED.JPG FRED.Jpg
             http://a.b.c/x/y/z/fred.gif }
    bad = %w{ fred.doc fred.gif/more fred.gif.more }
    ok.each do |name|
      assert new_news_item(name).valid?, "#{name} shouldn't be invalid"
    end

    bad.each do |name|
      assert new_news_item(name).invalid?, "#{name} shouldn't be valid"
    end
  end

end
