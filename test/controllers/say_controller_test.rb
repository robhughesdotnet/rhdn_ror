require 'test_helper'

class SayControllerTest < ActionController::TestCase
  test "should get hello" do
    get :hello
    assert_response :success
  end

  test "should get goodbye" do
    get :goodbye
    assert_response :success
  end

  test "should get news items" do
    get :hello
    assert_response :success
    assert_select '#main .entry', 2
    assert_select 'h3', '2008-09-17'
    assert_select 'h3', /^\d{4}-\d{2}-\d{2}$/
  end
end
