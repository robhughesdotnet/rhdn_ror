require 'test_helper'

class NewsItemsControllerTest < ActionController::TestCase
NewsItem.create(content:
%{<p>
Some random stuff I just had to share.
<br>
See  <a href="/gallery/2009_Colorado/2009_Colorado/album/index.html">here</a> 
</p>
},
image_url: 'https://s3.amazonaws.com/robhughesdotnet/gallery/website/NothingToSeeHere.png',
published_on: DateTime.new(2009,10,15,12))
  setup do
    @news_item = news_items(:one)
    @update = {
      content: 'Some random test data',
      image_url: 'https://s3.amazonaws.com/robhughesdotnet/gallery/website/NothingToSeeHere.png',
      published_on: DateTime.new(2009,10,15,12)
    }
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create news_item" do
    assert_difference('NewsItem.count') do
      post :create, news_item: @update

    end

    assert_redirected_to news_item_path(assigns(:news_item))
  end

  test "should show news_item" do
    get :show, id: @news_item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @news_item
    assert_response :success
  end

  test "should update news_item" do
    patch :update, id: @news_item, news_item: @update
    assert_redirected_to news_item_path(assigns(:news_item))
  end

  test "should destroy news_item" do
    assert_difference('NewsItem.count', -1) do
      delete :destroy, id: @news_item
    end

    assert_redirected_to news_items_path
  end
end
