class SayController < ApplicationController
  skip_before_action :authorize

  def hello
    #@news_items = NewsItem.order(:published_on)
    @news_items = NewsItem.order(published_on: :desc)
  end

  def photos
  end

  def goodbye
  end
end
