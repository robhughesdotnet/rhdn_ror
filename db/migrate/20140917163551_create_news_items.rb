class CreateNewsItems < ActiveRecord::Migration
  def change
    create_table :news_items do |t|
      t.text :content
      t.date :published_on
      t.string :image_url

      t.timestamps
    end
  end
end
