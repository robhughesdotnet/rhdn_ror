# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
NewsItem.delete_all
# . . .
NewsItem.create(content:
%{<p>
Posted gallery for mountain biking trip.
<br>
See  <a href="/gallery/2009_Colorado/2009_Colorado/album/index.html">here</a> 
</p>
},
image_url: 'https://s3.amazonaws.com/robhughesdotnet/gallery/2011+Fruita+CO+Moab+UT+Mountain+Bike+Trip/a18_Road/IMG_1261.JPG',
published_on: DateTime.new(2009,10,15,12))
#
NewsItem.create(content:
%{<p>
Some random stuff I just had to share.
<br>
See  <a href="/gallery/2009_Colorado/2009_Colorado/album/index.html">here</a> 
</p>
},
image_url: 'https://s3.amazonaws.com/robhughesdotnet/gallery/website/NothingToSeeHere.png',
published_on: DateTime.new(2009,10,15,12))
#
NewsItem.create(content:
%{<p>
RobHughes.net moves to the cloud!
</p>
},
image_url: 'https://s3.amazonaws.com/robhughesdotnet/gallery/website/aws_web_services.png',
published_on: DateTime.new(2014,9,17,12))
